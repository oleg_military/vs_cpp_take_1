﻿#include <iostream>
#include "Helpers.h"

int main()
{
	setlocale(LC_ALL, "Russian");

	try
	{
		double a = GetSumSquared(3, 4);
		int b = GetSumSquared(3.3, 4.2);

		std::cout << "\t - Возведенная в квадрат сумма 3 и 4:\t\t" << a << "\n";
		std::cout << "\t - Возведенная в квадрат сумма 3.3 и 4.2:\t" << b << "\n";
	}
	catch (std::exception* e)
	{
		std::cerr << e->what() << "\n";
	}
}