#pragma once
#include <iostream>
#include <exception>

/**
*	@param T - ��� ��� �������� ��� �������
*	@param [in] a <T> ������ ��������
*	@param [in] b <T> ������ ��������
*	@return [out] <T> ����������� � ������� ����� a � b
*/
template <typename T>
T GetSumSquared(T a, T b)
{
	return (a + b) * (a + b);
}
